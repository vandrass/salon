﻿namespace Final_project___Salon
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblClientsNumber = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblEmployees = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblServiceNumber = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblIncomeAllTime = new System.Windows.Forms.Label();
            this.lblIncomeYear = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblIncomeMonth = new System.Windows.Forms.Label();
            this.ChartsTabControl = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.CustomerChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.EmployeeStatChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.EmployeeChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ServiceChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.RightPanel = new System.Windows.Forms.Panel();
            this.HomeStatisticDataGridView = new System.Windows.Forms.DataGridView();
            this.DailyTaskstextBox = new System.Windows.Forms.TextBox();
            this.FooterPanel = new System.Windows.Forms.Panel();
            this.daybooklabel = new System.Windows.Forms.Label();
            this.TodaySquedulingLAbel = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPerformedServices = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblTotalWorkTime = new System.Windows.Forms.Label();
            this.clock1 = new Final_project___Salon.clock();
            this.employeeWorkerFormBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.ChartsTabControl.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerChart)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeStatChart)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeChart)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceChart)).BeginInit();
            this.RightPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HomeStatisticDataGridView)).BeginInit();
            this.FooterPanel.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeWorkerFormBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(176)))));
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Century Gothic", 20.25F);
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(1900, 71);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Hello Admin";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.lblTitle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1900, 71);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.lblClientsNumber);
            this.panel2.Location = new System.Drawing.Point(45, 98);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(186, 172);
            this.panel2.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(4, 4);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(85, 68);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(4, 122);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Our Clients";
            // 
            // lblClientsNumber
            // 
            this.lblClientsNumber.AutoSize = true;
            this.lblClientsNumber.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblClientsNumber.Location = new System.Drawing.Point(4, 75);
            this.lblClientsNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblClientsNumber.Name = "lblClientsNumber";
            this.lblClientsNumber.Size = new System.Drawing.Size(42, 47);
            this.lblClientsNumber.TabIndex = 0;
            this.lblClientsNumber.Text = "0";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.pictureBox3);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.lblEmployees);
            this.panel3.Location = new System.Drawing.Point(240, 98);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(186, 172);
            this.panel3.TabIndex = 7;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(4, 4);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(85, 68);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(4, 122);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 26);
            this.label4.TabIndex = 4;
            this.label4.Text = "Employees";
            // 
            // lblEmployees
            // 
            this.lblEmployees.AutoSize = true;
            this.lblEmployees.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblEmployees.Location = new System.Drawing.Point(17, 75);
            this.lblEmployees.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmployees.Name = "lblEmployees";
            this.lblEmployees.Size = new System.Drawing.Size(42, 47);
            this.lblEmployees.TabIndex = 4;
            this.lblEmployees.Text = "0";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.lblServiceNumber);
            this.panel4.Location = new System.Drawing.Point(435, 98);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(186, 172);
            this.panel4.TabIndex = 7;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 4);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(85, 68);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(4, 122);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 26);
            this.label3.TabIndex = 3;
            this.label3.Text = "Services";
            // 
            // lblServiceNumber
            // 
            this.lblServiceNumber.AutoSize = true;
            this.lblServiceNumber.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblServiceNumber.Location = new System.Drawing.Point(16, 75);
            this.lblServiceNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblServiceNumber.Name = "lblServiceNumber";
            this.lblServiceNumber.Size = new System.Drawing.Size(42, 47);
            this.lblServiceNumber.TabIndex = 1;
            this.lblServiceNumber.Text = "0";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            this.panel5.Controls.Add(this.lblIncomeAllTime);
            this.panel5.Controls.Add(this.lblIncomeYear);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.pictureBox4);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.lblIncomeMonth);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(824, 268);
            this.panel5.TabIndex = 8;
            // 
            // lblIncomeAllTime
            // 
            this.lblIncomeAllTime.AutoSize = true;
            this.lblIncomeAllTime.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIncomeAllTime.Location = new System.Drawing.Point(263, 194);
            this.lblIncomeAllTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIncomeAllTime.Name = "lblIncomeAllTime";
            this.lblIncomeAllTime.Size = new System.Drawing.Size(174, 47);
            this.lblIncomeAllTime.TabIndex = 9;
            this.lblIncomeAllTime.Text = "1000000";
            // 
            // lblIncomeYear
            // 
            this.lblIncomeYear.AutoSize = true;
            this.lblIncomeYear.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIncomeYear.Location = new System.Drawing.Point(263, 135);
            this.lblIncomeYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIncomeYear.Name = "lblIncomeYear";
            this.lblIncomeYear.Size = new System.Drawing.Size(174, 47);
            this.lblIncomeYear.TabIndex = 8;
            this.lblIncomeYear.Text = "1000000";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(16, 213);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 26);
            this.label8.TabIndex = 7;
            this.label8.Text = "All Time";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(16, 155);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 26);
            this.label7.TabIndex = 6;
            this.label7.Text = "Last Year";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 18.25F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(139, 15);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 37);
            this.label6.TabIndex = 5;
            this.label6.Text = "Income";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(21, 4);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(109, 91);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(16, 98);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "Last Month";
            // 
            // lblIncomeMonth
            // 
            this.lblIncomeMonth.AutoSize = true;
            this.lblIncomeMonth.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIncomeMonth.Location = new System.Drawing.Point(263, 79);
            this.lblIncomeMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIncomeMonth.Name = "lblIncomeMonth";
            this.lblIncomeMonth.Size = new System.Drawing.Size(174, 47);
            this.lblIncomeMonth.TabIndex = 4;
            this.lblIncomeMonth.Text = "1000000";
            // 
            // ChartsTabControl
            // 
            this.ChartsTabControl.Controls.Add(this.tabPage3);
            this.ChartsTabControl.Controls.Add(this.tabPage4);
            this.ChartsTabControl.Controls.Add(this.tabPage1);
            this.ChartsTabControl.Controls.Add(this.tabPage2);
            this.ChartsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartsTabControl.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold);
            this.ChartsTabControl.Location = new System.Drawing.Point(0, 268);
            this.ChartsTabControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ChartsTabControl.Name = "ChartsTabControl";
            this.ChartsTabControl.SelectedIndex = 0;
            this.ChartsTabControl.Size = new System.Drawing.Size(824, 486);
            this.ChartsTabControl.TabIndex = 14;
            this.ChartsTabControl.Tag = "";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.CustomerChart);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(816, 448);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Customers statistic";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // CustomerChart
            // 
            this.CustomerChart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            chartArea1.AxisX.Interval = 1D;
            chartArea1.Name = "ChartArea1";
            this.CustomerChart.ChartAreas.Add(chartArea1);
            this.CustomerChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.CustomerChart.Legends.Add(legend1);
            this.CustomerChart.Location = new System.Drawing.Point(0, 0);
            this.CustomerChart.Margin = new System.Windows.Forms.Padding(4);
            this.CustomerChart.Name = "CustomerChart";
            this.CustomerChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Count of services";
            this.CustomerChart.Series.Add(series1);
            this.CustomerChart.Size = new System.Drawing.Size(816, 448);
            this.CustomerChart.TabIndex = 11;
            this.CustomerChart.Text = "Employee salary";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.EmployeeStatChart);
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(816, 448);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Best employee";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // EmployeeStatChart
            // 
            this.EmployeeStatChart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            chartArea2.AxisX.Interval = 1D;
            chartArea2.Name = "ChartArea1";
            this.EmployeeStatChart.ChartAreas.Add(chartArea2);
            this.EmployeeStatChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.EmployeeStatChart.Legends.Add(legend2);
            this.EmployeeStatChart.Location = new System.Drawing.Point(0, 0);
            this.EmployeeStatChart.Margin = new System.Windows.Forms.Padding(4);
            this.EmployeeStatChart.Name = "EmployeeStatChart";
            this.EmployeeStatChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series2.Legend = "Legend1";
            series2.LegendText = "#PERCENT";
            series2.Name = "Count of clients";
            this.EmployeeStatChart.Series.Add(series2);
            this.EmployeeStatChart.Size = new System.Drawing.Size(816, 448);
            this.EmployeeStatChart.TabIndex = 12;
            this.EmployeeStatChart.Text = "Employee salary";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.EmployeeChart);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(816, 448);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Employee salary";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // EmployeeChart
            // 
            this.EmployeeChart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            chartArea3.AxisX.Interval = 1D;
            chartArea3.Name = "ChartArea1";
            this.EmployeeChart.ChartAreas.Add(chartArea3);
            this.EmployeeChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend3.Name = "Legend1";
            this.EmployeeChart.Legends.Add(legend3);
            this.EmployeeChart.Location = new System.Drawing.Point(3, 2);
            this.EmployeeChart.Margin = new System.Windows.Forms.Padding(4);
            this.EmployeeChart.Name = "EmployeeChart";
            this.EmployeeChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Berry;
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Salary";
            this.EmployeeChart.Series.Add(series3);
            this.EmployeeChart.Size = new System.Drawing.Size(810, 444);
            this.EmployeeChart.TabIndex = 9;
            this.EmployeeChart.Text = "Employee salary";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ServiceChart);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(816, 448);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Service prices";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ServiceChart
            // 
            this.ServiceChart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            chartArea4.AxisX.Interval = 1D;
            chartArea4.Name = "ChartArea1";
            this.ServiceChart.ChartAreas.Add(chartArea4);
            this.ServiceChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend4.Name = "Legend1";
            this.ServiceChart.Legends.Add(legend4);
            this.ServiceChart.Location = new System.Drawing.Point(3, 2);
            this.ServiceChart.Margin = new System.Windows.Forms.Padding(4);
            this.ServiceChart.Name = "ServiceChart";
            this.ServiceChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.EarthTones;
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Price";
            this.ServiceChart.Series.Add(series4);
            this.ServiceChart.Size = new System.Drawing.Size(810, 444);
            this.ServiceChart.TabIndex = 10;
            this.ServiceChart.Text = "Employee salary";
            // 
            // RightPanel
            // 
            this.RightPanel.Controls.Add(this.ChartsTabControl);
            this.RightPanel.Controls.Add(this.panel5);
            this.RightPanel.Controls.Add(this.clock1);
            this.RightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightPanel.Location = new System.Drawing.Point(1076, 71);
            this.RightPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RightPanel.Name = "RightPanel";
            this.RightPanel.Size = new System.Drawing.Size(824, 804);
            this.RightPanel.TabIndex = 15;
            // 
            // HomeStatisticDataGridView
            // 
            this.HomeStatisticDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HomeStatisticDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.HomeStatisticDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(239)))), ((int)(((byte)(254)))));
            this.HomeStatisticDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HomeStatisticDataGridView.Location = new System.Drawing.Point(16, 342);
            this.HomeStatisticDataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.HomeStatisticDataGridView.Name = "HomeStatisticDataGridView";
            this.HomeStatisticDataGridView.RowHeadersWidth = 51;
            this.HomeStatisticDataGridView.RowTemplate.Height = 24;
            this.HomeStatisticDataGridView.Size = new System.Drawing.Size(673, 359);
            this.HomeStatisticDataGridView.TabIndex = 16;
            // 
            // DailyTaskstextBox
            // 
            this.DailyTaskstextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DailyTaskstextBox.Location = new System.Drawing.Point(0, 29);
            this.DailyTaskstextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DailyTaskstextBox.Multiline = true;
            this.DailyTaskstextBox.Name = "DailyTaskstextBox";
            this.DailyTaskstextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DailyTaskstextBox.Size = new System.Drawing.Size(1076, 136);
            this.DailyTaskstextBox.TabIndex = 17;
            // 
            // FooterPanel
            // 
            this.FooterPanel.Controls.Add(this.daybooklabel);
            this.FooterPanel.Controls.Add(this.DailyTaskstextBox);
            this.FooterPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.FooterPanel.Location = new System.Drawing.Point(0, 710);
            this.FooterPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FooterPanel.Name = "FooterPanel";
            this.FooterPanel.Size = new System.Drawing.Size(1076, 165);
            this.FooterPanel.TabIndex = 18;
            // 
            // daybooklabel
            // 
            this.daybooklabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.daybooklabel.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold);
            this.daybooklabel.Location = new System.Drawing.Point(0, 0);
            this.daybooklabel.Name = "daybooklabel";
            this.daybooklabel.Size = new System.Drawing.Size(1076, 26);
            this.daybooklabel.TabIndex = 0;
            this.daybooklabel.Text = "Notes";
            this.daybooklabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TodaySquedulingLAbel
            // 
            this.TodaySquedulingLAbel.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold);
            this.TodaySquedulingLAbel.Location = new System.Drawing.Point(452, 305);
            this.TodaySquedulingLAbel.Name = "TodaySquedulingLAbel";
            this.TodaySquedulingLAbel.Size = new System.Drawing.Size(213, 34);
            this.TodaySquedulingLAbel.TabIndex = 19;
            this.TodaySquedulingLAbel.Text = "Today Squeduling ";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.pictureBox5);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.lblPerformedServices);
            this.panel6.Location = new System.Drawing.Point(629, 98);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(186, 172);
            this.panel6.TabIndex = 8;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(4, 4);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(85, 68);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(4, 116);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 46);
            this.label5.TabIndex = 4;
            this.label5.Text = "Performed \r\nServices";
            // 
            // lblPerformedServices
            // 
            this.lblPerformedServices.AutoSize = true;
            this.lblPerformedServices.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPerformedServices.Location = new System.Drawing.Point(17, 75);
            this.lblPerformedServices.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPerformedServices.Name = "lblPerformedServices";
            this.lblPerformedServices.Size = new System.Drawing.Size(42, 47);
            this.lblPerformedServices.TabIndex = 4;
            this.lblPerformedServices.Text = "0";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(246)))), ((int)(((byte)(228)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.pictureBox6);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.lblTotalWorkTime);
            this.panel7.Location = new System.Drawing.Point(824, 98);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(186, 172);
            this.panel7.TabIndex = 9;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(4, 4);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(85, 68);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 4;
            this.pictureBox6.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(4, 123);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 23);
            this.label10.TabIndex = 4;
            this.label10.Text = "Total Work Time";
            // 
            // lblTotalWorkTime
            // 
            this.lblTotalWorkTime.AutoSize = true;
            this.lblTotalWorkTime.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTotalWorkTime.Location = new System.Drawing.Point(4, 75);
            this.lblTotalWorkTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalWorkTime.Name = "lblTotalWorkTime";
            this.lblTotalWorkTime.Size = new System.Drawing.Size(77, 47);
            this.lblTotalWorkTime.TabIndex = 4;
            this.lblTotalWorkTime.Text = "0 h";
            // 
            // clock1
            // 
            this.clock1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(196)))), ((int)(((byte)(233)))));
            this.clock1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.clock1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.clock1.Location = new System.Drawing.Point(0, 754);
            this.clock1.Margin = new System.Windows.Forms.Padding(0);
            this.clock1.Name = "clock1";
            this.clock1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.clock1.Size = new System.Drawing.Size(824, 50);
            this.clock1.TabIndex = 4;
            // 
            // employeeWorkerFormBindingSource
            // 
            this.employeeWorkerFormBindingSource.DataSource = typeof(Final_project___Salon.EmployeeWorkerForm);
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(218)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(1900, 875);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.TodaySquedulingLAbel);
            this.Controls.Add(this.FooterPanel);
            this.Controls.Add(this.HomeStatisticDataGridView);
            this.Controls.Add(this.RightPanel);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "HomeForm";
            this.Text = "HomeForm";
            this.TransparencyKey = System.Drawing.Color.DimGray;
            this.Load += new System.EventHandler(this.HomeForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ChartsTabControl.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustomerChart)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeStatChart)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeChart)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ServiceChart)).EndInit();
            this.RightPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HomeStatisticDataGridView)).EndInit();
            this.FooterPanel.ResumeLayout(false);
            this.FooterPanel.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeWorkerFormBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel panel1;
        private clock clock1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblClientsNumber;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblServiceNumber;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblEmployees;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblIncomeAllTime;
        private System.Windows.Forms.Label lblIncomeYear;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblIncomeMonth;
        private System.Windows.Forms.BindingSource employeeWorkerFormBindingSource;
        private System.Windows.Forms.TabControl ChartsTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataVisualization.Charting.Chart EmployeeChart;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataVisualization.Charting.Chart ServiceChart;
        private System.Windows.Forms.Panel RightPanel;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataVisualization.Charting.Chart CustomerChart;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataVisualization.Charting.Chart EmployeeStatChart;
        private System.Windows.Forms.DataGridView HomeStatisticDataGridView;
        private System.Windows.Forms.TextBox DailyTaskstextBox;
        private System.Windows.Forms.Panel FooterPanel;
        private System.Windows.Forms.Label daybooklabel;
        private System.Windows.Forms.Label TodaySquedulingLAbel;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPerformedServices;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTotalWorkTime;
    }
}