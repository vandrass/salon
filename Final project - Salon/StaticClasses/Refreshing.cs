﻿
using System.Windows.Forms;

namespace Final_project___Salon
{
    /*
     Class for data refreshing in other forms from current form
     */
    public static class Refreshing
    {


        /*
         * Method refresh timeTable
         */
        public static void TimeTableRefresh()
        {
            var home = (HomeForm)Application.OpenForms["HomeForm"];
            home.FillTimeTable();
            var scuedul = (Scueduling)Application.OpenForms["Scueduling"];
            scuedul.FillTimeTable();


        }

    }
}
